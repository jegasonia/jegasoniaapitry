import Vue from 'vue'
import Router from 'vue-router'
import MaPage from '@/components/mapage'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'mapage',
    component: MaPage
  }]
})
